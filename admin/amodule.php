<?

/**
 * @Author	: Andrew Berezovskiy (andrew.berezovskiy@gmail.com)
 * @Date	: 15.02.08
 * @Lastmod	: 10.07.08
 */
define('MODULE_INCLUDE', true);
ob_start(ob_gzhandler);
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
$output='';

/**
 * Include conf and classes
 */
function __autoload($name) {
	$name = strtolower($name);
	@include_once($_SERVER['DOCUMENT_ROOT'].'/classes/class.'.$name.'.php');
}
include $_SERVER['DOCUMENT_ROOT'].'/config.php';
//include ROOT.'/classes/class.profiler.php';
$profiler=new Time;
$profiler->start();
include ROOT.'/classes/class.mysql.php';
include ROOT.'/classes/class.smarty.php';

/**
 * Establish MySQL connection
 */
$db = new DB;
$db->connect(HOST, USER, PASS);
$db->select_db(DBNAME);

/**
 * Template engine
 */
$smarty = new Smarty();

$smarty->template_dir	= ROOT.'/smarty/templates/'.TEMPLATE_NAME;
$smarty->compile_dir	= ROOT.'/smarty/templates_c';
$smarty->cache_dir		= ROOT.'/smarty/cache';
$smarty->config_dir		= ROOT.'/smarty/configs';

/**
 * 1.������ ������� ����������
 * 2.������� �������� $_GET � $_POST �� ������ ������� ��������
 */
array_walk($_GET, mytrim);
array_walk($_POST, mytrim);
function mytrim(&$val, $key) {
	$val = trim($val);
}
$module	= $_GET['module'];
$action	= $_GET['action'];
if(empty($module))
{
    $module = 'bccount';
}
$smarty->assign('module', $module);
$smarty->assign('action', $action);

/**
 * Code start point
 * Include the module
 */

$path="./modules/$module/module.$module.php";
if(!file_exists($path)) header("HTTP/1.0 404 Not Found");
else include_once $path;

/**
 * Code end point
 * Process the output
 */

$smarty->assign('output', $output);
$smarty->assign('exectime', $profiler->end(4, true));
$smarty->assign('execqueries', $queries);

echo $smarty->fetch('index.tpl');
ob_end_flush();
mysql_close();
exit;
?>
