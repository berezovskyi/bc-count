<?php

#General
define('SITE'         , $_SERVER['HTTP_HOST']);
define('ROOT'         , $_SERVER['DOCUMENT_ROOT']);

#MyQSL Database
define('HOST'         , 'localhost');
define('USER'         , 'root');
define('PASS'         , '');
define('DBNAME'       , 'bccount');
define('PREFIX'       , 'bc_');

#Settings
define('SMARTY_DIR'   , $_SERVER['DOCUMENT_ROOT'].'/smarty/');
define('TEMPLATE_NAME', 'default');

?>