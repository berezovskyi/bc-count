<?php

/**
 * @author Andrew Berezovskiy
 * @copyright 2008
 */

session_start();
if(!(strlen($_SESSION['step']) > 0)) {
	$step = 1;
}
else {
	$step = $_SESSION['step'];
}
/**
 * Step 1: Ask for params
 */
 
 /**
 * Step 2: Check them
 */

/**
 * Step 3: Submit data
 */
 
 $tbl_banners = 'CREATE TABLE `'.$prefix.'banners` ('
        . ' `id` INT(5) NOT NULL AUTO_INCREMENT, '
        . ' `name` VARCHAR(200) NOT NULL, '
        . ' `url` VARCHAR(100) NOT NULL, '
        . ' `hash` VARCHAR(32) NOT NULL, '
        . ' `time` BIGINT(20) NOT NULL,'
        . ' PRIMARY KEY (`id`)'
        . ' )';
 
 $tbl_count = 'CREATE TABLE `'.$prefix.'count` ('
        . ' `id` INT(5) NOT NULL AUTO_INCREMENT, '
        . ' `banner` INT(5) NOT NULL, '
        . ' `ip` VARCHAR(15) NOT NULL, '
        . ' `time` BIGINT(20) NOT NULL,'
        . ' PRIMARY KEY (`id`)'
        . ' )';
 
 /**
 * Step 4: Finish
 */

?>