<?php

/**
 * @author Andrew Berezovskiy <andrew.berezovskiy@gmail.com>
 * @package BannerClickCounter
 */

include "./config.php";
include "./classes/class.mysql.php";

$db = new DB;
$db->connect(HOST, USER, PASS);
$db->select_db(DBNAME);

/**
 * Reveal a requested banner url
 */
$hash = $_GET['go'];
$hash = mysql_real_escape_string($hash);
$sql = "SELECT id, url FROM ".PREFIX."banners WHERE hash = '$hash'";
$cReply = $db->select($sql);
$id = $cReply->id;
$url = $cReply->url;

/**
 * Check if this user have previously requested this page
 */
$ip = $_SERVER['REMOTE_ADDR'];
$interval = time() - 3600*48;
$sql = "SELECT count('ip') as num FROM ".PREFIX."count WHERE id = $id AND ip = '$ip' AND time > ".$interval;
if($db->select($sql)->num > 0) {
	// Don't count the visit
	$count = false;
}


/**
 * Submit a hit
 */

if($count !== false) {
	$sql = "INSERT INTO ".PREFIX."count VALUES(NULL, $id, '$ip', ".time().")";
	$db->query($sql);
}

header("Location: $url");
?>
