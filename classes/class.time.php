<?
/**
 *This class is used to measure
 *code execution time
 */

class Time {
	private $start_time;
	var $chars;

	/**
	 *Internal function used to
	 *get the time now
	 */
	private function get_time() {
		list($usec, $sec) = explode(' ', microtime());
		return ((float)$usec + (float)$sec);
	}
	/**
	 *Use it to start the timer
	 */
	function start() {
		$this->start_time = $this->get_time();
	}
	/**
	 *Use it to end the timer
	 *@param int $a Chars after the comma
	 *@param bool $msec Boolean if you want a response in msecs
	 *@return float Time from the start
	 */
	function end($a, $msec = false) {
		$chars=$a + $this->chars;
		$diff = $this->get_time() - $this->start_time;
		$secs=explode(".", $diff);
		$this->chars = strlen($secs['0']);
		if(!$msec) {
			return substr($diff, 0, $chars);
		}
		else {
			return round($diff*1000);
		}
	}
	/**
	 *Resets the timer
	 */
	function reset() {
		$this->start_time = 0;
	}

}
?>
